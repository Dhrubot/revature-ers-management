window.onload = async () => {
    if (!token) {
        window.location.href = BASE_URL + "/auth"
    } else if (token1 && token1.split(":")[1] !== "EMPLOYEE") {
        window.location.href = BASE_URL + "/static/errorPage.html"
    } else {
        fetchPendingReimbursements();
    }
};
const token1 = sessionStorage.getItem("token");

let displayReimbursements = reimbursements => {
    console.log(reimbursements)
    const columnHeadings = ['Requested By', 'amount', 'description', 'status', 'Request Date'];
    const columnCount = columnHeadings.length;
    const rowCount = reimbursements.length;


    const header = table.createTHead();

    let row = header.insertRow(-1);

    for (let i = 0; i < columnCount; i++) {

        let headerCell = document.createElement('th');

        headerCell.innerText = columnHeadings[i].toUpperCase();

        row.appendChild(headerCell);

    }

    let tBody = document.createElement('tbody');

    table.appendChild(tBody);

    // Add the data rows to the table body.

    for (let i = 0; i < reimbursements.length; i++) { // each row

        row = tBody.insertRow(-1);

        let objFields = ['firstname', 'amount', 'description', 'status', 'createdOn']


        let obj = reimbursements[i];

        objFields.forEach(function (field) {
            let cell = row.insertCell(-1);
            if (field === "firstname") {
                cell.innerText = reimbursements[i].employee.firstName[0].toUpperCase() + reimbursements[i].employee.firstName.substring(1);
            }
            else if (field === "amount") {
                let money = new Number(reimbursements[i].amount);
                cell.innerText = `$ ${money.toFixed(2)}`;
            }
            else if (field === 'status') {
                cell.innerText = reimbursements[i].resolved ? 'APPROVED' : 'PENDING';
                cell.style.color = reimbursements[i].iresolved ? 'GREEN' : 'RED';
            }
            else if (field === "createdOn") {
                let date = new Date(reimbursements[i][field]);
                cell.innerText = (date.getMonth() + 1) + "-" + date.getDate() + "-" + date.getFullYear()
            } else {
                cell.innerText = reimbursements[i][field];
            }
        })
    }
}

function fetchPendingReimbursements() {
    if (token1 && token1.split(":")[1] === "EMPLOYEE") {
        fetch(BASE_URL + "/reimbursements", { headers: { "Authorization": token1, "filter": "pending" } })
            .then(res => res.json())
            .then(data => {
                if (data.length) {
                    displayReimbursements(data)
                } else {
                    let tBody = document.createElement('tbody');
                    table.appendChild(tBody);
                    row = tBody.insertRow(-1);
                    row.innerHTML = `<h4> No Pending Reimbursement</h4>`
                    document.querySelector("tr").style.textAlign = "center";
                }
            })
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}