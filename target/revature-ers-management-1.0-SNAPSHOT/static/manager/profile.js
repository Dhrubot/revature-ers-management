window.onload = async () => {

    if (!token) {
        window.location.href = BASE_URL + "/auth"
    } else if (token1 && token1.split(":")[1] !== "MANAGER") {
        window.location.href = BASE_URL + "/static/errorPage.html"
    } else {
        fetchProfile();
    }
};

const token1 = sessionStorage.getItem("token");

function fetchProfile() {
    if (token1) {
        fetch(BASE_URL + "/profile", { headers: { "Authorization": token1 } })
            .then(res => res.json())
            .then(data => displayProfile(data.user))
    }
}

function displayProfile(user) {
    const fullNameDiv = document.getElementById("user-fullName");
    const roleDiv = document.getElementById("user-role");
    const usernameDiv = document.getElementById("user-username");
    const emailDiv = document.getElementById("user-email");
    fullNameDiv.innerText = nameTitleCase(user.firstName, user.lastName)
    console.log(user.firstName)
    roleDiv.innerText = nameTitleCase(user.userRole)
    usernameDiv.innerText = user.username
    emailDiv.innerText = user.email
}

function nameTitleCase(firstname, lastname = "") {
    if (lastname) {
        return firstname[0].toUpperCase() + firstname.substring(1)
            + " " +
            lastname[0].toUpperCase() + lastname.substring(1)
    } else {
        return firstname[0].toUpperCase() + firstname.substring(1).toLowerCase()
    }
}