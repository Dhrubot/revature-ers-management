window.onload = async () => {
    if (!token) {
        window.location.href = BASE_URL + "/auth"
    } else if (token1 && token1.split(":")[1] !== "MANAGER") {
        console.log("I am manager coming here")
        window.location.href = BASE_URL + "/static/errorPage.html"
    } else {
        console.log("I am coming here")
        fetchEmployees();
    }
};
const token1 = sessionStorage.getItem("token");

let displayUsers = users => {
    const columnHeadings = ['firstName', 'lastName', 'email'];
    const columnCount = columnHeadings.length;
    const rowCount = users.length;


    const header = table.createTHead();

    let row = header.insertRow(-1);

    for (let i = 0; i < columnCount; i++) {

        let headerCell = document.createElement('th');

        headerCell.innerText = columnHeadings[i].toUpperCase();

        row.appendChild(headerCell);

    }

    let tBody = document.createElement('tbody');

    table.appendChild(tBody);

    // Add the data rows to the table body.

    for (let i = 0; i < rowCount; i++) { // each row

        row = tBody.insertRow(-1);

        for (let j = 0; j < columnCount; j++) { // each column

            let cell = row.insertCell(-1);

            cell.setAttribute('data-label', columnHeadings[j].toUpperCase());

            let obj = users[i];

            cell.innerText = obj[columnHeadings[j]];

        }
    }
}

function fetchEmployees() {
    if (token1 && token1.split(":")[1] === "MANAGER") {
        fetch(BASE_URL + "/users", { headers: { "Authorization": token1 } })
            .then(res => res.json())
            .then(data => displayUsers(data))
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}