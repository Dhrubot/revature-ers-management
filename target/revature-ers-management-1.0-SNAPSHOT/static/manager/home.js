document.getElementById("view-employees").addEventListener("click", goToEmployees);
document.getElementById("home").addEventListener("click", goToHome);
document.getElementById("pending-reimbursements").addEventListener("click", goToPendingReimbursements);
document.getElementById("resolved-reimbursements").addEventListener("click", goToResolvedReimbursements);
document.getElementById("profile").addEventListener("click", goToProfile);
document.getElementById("logout").addEventListener("click", logoutUser);
const token = sessionStorage.getItem("token");

//const BASE_URL = "http://13.58.47.140:8080/revature-ers-management-1.0-SNAPSHOT"
const BASE_URL = "http://localhost:8080/revature-ers-management"


function goToEmployees() {
    if (token && token.split(":")[1] === "MANAGER") {
        window.location.href = BASE_URL + "/static/manager/viewEmployees.html";
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}

function goToHome() {
    if (token && token.split(":")[1] === "MANAGER") {
        window.location.href = BASE_URL + "/static/manager/home.html";
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}
function goToPendingReimbursements() {
    if (token && token.split(":")[1] === "MANAGER") {
        window.location.href = BASE_URL + "/static/manager/pendingReimbursements.html";
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}

function goToResolvedReimbursements() {
    if (token && token.split(":")[1] === "MANAGER") {
        window.location.href = BASE_URL + "/static/manager/resolvedReimbursements.html";
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}

function goToProfile() {
    if (token && token.split(":")[1] === "MANAGER") {
        window.location.href = BASE_URL + "/static/manager/profile.html";
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}

function logoutUser() {
    if (token) {
        fetch(BASE_URL + "/logout", { headers: { "Authorization": token } })
            .then(res => {
                sessionStorage.clear();
                window.location.href = BASE_URL + "/auth"
            })
    }
}

