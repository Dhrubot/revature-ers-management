package com.revature.ers.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.ers.model.User;
import com.revature.ers.model.UserRole;
import com.revature.ers.service.AuthService;
import com.revature.ers.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class UserServlet extends HttpServlet {

    private AuthService authService = new AuthService();
    private UserService userService = new UserService();
    private final Logger logger = Logger.getLogger(UserServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String authToken = req.getHeader("Authorization");
        boolean validToken = authService.validateToken(authToken);
        if (!validToken) {
            resp.sendError(400,"Improper Format");
            logger.warn("Invalid client token");
        } else {
            User currentUser = authService.findUserByToken(authToken);

            if (currentUser==null){
                resp.sendError(401, "No User Found");
                logger.warn("No user found");
            } else {
                if (currentUser.getUserRole() == UserRole.MANAGER) {
                    resp.setStatus(200);

                    try (PrintWriter pw = resp.getWriter();) {
                        List<User> users = userService.getAllEmployees();
                        ObjectMapper om = new ObjectMapper();
                        String jsonData = om.writeValueAsString(users);
                        pw.write(jsonData);
                    }
                } else {
                    resp.sendError(403, "Not Authorized");
                    logger.warn("No proper role for user");
                }
            }
        }
    }
}
