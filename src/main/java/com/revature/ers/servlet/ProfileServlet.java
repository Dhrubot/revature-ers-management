package com.revature.ers.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.ers.model.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class ProfileServlet extends HttpServlet {
    private final Logger logger = Logger.getLogger(ProfileServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            try (PrintWriter pw = resp.getWriter();) {
                User user = (User) session.getAttribute("user");
                HashMap<String, User> userData = new HashMap<>();
                if (user != null) {
                    resp.setStatus(200);
                    userData.put("user", user);
                } else {
                    resp.setStatus(404);
                    userData.put("user", null);
                    logger.warn("No user found in session.");
                }
                ObjectMapper om = new ObjectMapper();
                String jsonData = om.writeValueAsString(userData);
                pw.write(jsonData);
            }

        } else {
            logger.warn("Session is null");
            resp.sendRedirect("static/login.html");
        }
    }
}
