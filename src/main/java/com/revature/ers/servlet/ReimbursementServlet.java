package com.revature.ers.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.ers.model.Reimbursement;
import com.revature.ers.model.User;
import com.revature.ers.model.UserRole;
import com.revature.ers.service.AuthService;
import com.revature.ers.service.ReimbursementService;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.JSONTokener;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.HashMap;
import java.util.List;

public class ReimbursementServlet extends HttpServlet {
    private AuthService authService = new AuthService();
    private ReimbursementService reimbursementService = new ReimbursementService();
    private final Logger logger = Logger.getLogger(ReimbursementServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String authToken = req.getHeader("Authorization");
        String filterReq = req.getHeader("filter");
        boolean validToken = authService.validateToken(authToken);
        if (!validToken) {
            resp.sendError(400,"Improper Format");
            logger.warn("Improper client token");
        } else {
            User currentUser = authService.findUserByToken(authToken);

            if (currentUser==null){
                resp.sendError(401, "No User Found");
                logger.warn("No user found");
            } else {
                if (currentUser.getUserRole() == UserRole.MANAGER) {
                    resp.setStatus(200);

                    try (PrintWriter pw = resp.getWriter();) {
                        List<Reimbursement> reimbursementList;
                        if ("pending".equals(filterReq)) {
                            reimbursementList = reimbursementService.getAllPendingReimbursements();
                        } else if ("resolved".equals(filterReq)) {
                            reimbursementList = reimbursementService.getAllResolvedReimbursements();
                        } else {
                            reimbursementList = reimbursementService.getAllReimbursements();
                        }
                        ObjectMapper om = new ObjectMapper();
                        String jsonData = om.writeValueAsString(reimbursementList);
                        pw.write(jsonData);
                    }
                } else if (currentUser.getUserRole() == UserRole.EMPLOYEE) {
                    resp.setStatus(200);

                    try (PrintWriter pw = resp.getWriter();) {
                        List<Reimbursement> reimbursementList;
                        if ("pending".equals(filterReq)) {
                            reimbursementList = reimbursementService.getAllEmployeePendingReimbursements(currentUser);
                        } else if ("resolved".equals(filterReq)) {
                            reimbursementList = reimbursementService.getAllEmployeeResolvedReimbursements(currentUser);
                        } else {
                            reimbursementList = reimbursementService.getAllEmployeeReimbursements(currentUser);
                        }
                        ObjectMapper om = new ObjectMapper();
                        String jsonData = om.writeValueAsString(reimbursementList);
                        pw.write(jsonData);
                    }
                } else {
                    resp.sendError(403, "Not Authorized");
                    logger.warn("No proper role for user");
                }
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            InputStream requestBody = req.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(requestBody));
            JSONTokener tokener = new JSONTokener(bufferedReader);
            JSONObject json = new JSONObject(tokener);

            String authToken = req.getHeader("Authorization");
            boolean validToken = authService.validateToken(authToken);

            if (!validToken) {
                resp.sendError(400,"Improper Format");
            } else {
                User currentUser = (User) session.getAttribute("user");

                if (UserRole.EMPLOYEE.equals(currentUser.getUserRole())) {
                    resp.setStatus(201);
                    double amount = json.getDouble("amount");
                    String desc = json.getString("description");
                    try (PrintWriter pw = resp.getWriter();) {
                        HashMap<String, Boolean> success = new HashMap<>();
                        boolean createSuccess = reimbursementService.createReimbursement(amount, desc, currentUser);
                        if (createSuccess) {
                            success.put("created", true);
                        } else {
                            success.put("created", false);
                        }
                        ObjectMapper om = new ObjectMapper();
                        String jsonData = om.writeValueAsString(success);
                        pw.write(jsonData);
                    }
                } else {
                    resp.sendError(403, "Not Authorized");
                }
            }

        } else {
            resp.sendRedirect("static/login.html");
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InputStream requestBody = req.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(requestBody));
        JSONTokener tokener = new JSONTokener(bufferedReader);
        JSONObject json = new JSONObject(tokener);
        String authToken = req.getHeader("Authorization");
        boolean validToken = authService.validateToken(authToken);
        if (!validToken) {
            resp.sendError(400,"Improper Format");
        } else {
            User currentUser = authService.findUserByToken(authToken);

            if (currentUser == null) {
                resp.sendError(401, "No User Found");
            } else {
                if (currentUser.getUserRole() == UserRole.MANAGER && json.getBoolean("approve")) {
                    resp.setStatus(200);

                    try (PrintWriter pw = resp.getWriter();) {
                        HashMap<String, Boolean> success = new HashMap<>();
                        boolean updateSuccess = reimbursementService.approveReimbursement(reimbursementService.getReimbursementById(json.getInt("id")), currentUser);
                        if (updateSuccess) {
                            success.put("approved", true);
                        } else {
                            success.put("approved", false);
                        }
                        ObjectMapper om = new ObjectMapper();
                        String jsonData = om.writeValueAsString(success);
                        pw.write(jsonData);
                    }
                } else {
                    resp.sendError(403, "Not Authorized");
                }
            }
        }
    }
}
