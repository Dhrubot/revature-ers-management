package com.revature.ers.servlet;

import com.revature.ers.model.User;
import com.revature.ers.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthServlet extends HttpServlet {

    private UserService userService = new UserService();
    private final Logger logger = Logger.getLogger(AuthServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session != null) {
            String role = (String) session.getAttribute("role");
            if ("MANAGER".equals(role)) {
                resp.sendRedirect("static/manager/home.html");
            } else if ("EMPLOYEE".equals(role)) {
                resp.sendRedirect("static/employee/home.html");
            }
        } else {
            logger.info("Session is null. No user logged in.");
            resp.sendRedirect("static/login.html");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userNameParam = req.getParameter("username");
        String passwordParam = req.getParameter("password");

        User user = userService.getUserByCredentials(userNameParam, passwordParam);
        if (user.getId() == 0) {
            resp.sendError(401, "No Valid Account");
            logger.warn("Unsuccessful login. No user found");
        } else {
            resp.setStatus(200);
            HttpSession session = req.getSession();
            session.setAttribute("role", user.getUserRole().toString());
            session.setAttribute("user", user);
            String token = user.getId() + ":" + user.getUserRole();
            resp.setHeader("Authorization", token);
        }
    }
}
