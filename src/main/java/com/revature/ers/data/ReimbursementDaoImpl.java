package com.revature.ers.data;

import com.revature.ers.model.Reimbursement;
import com.revature.ers.model.User;
import com.revature.ers.model.UserRole;
import com.revature.ers.service.ConnectionService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementDaoImpl implements ReimbursementDao{
    private ConnectionService connectionService;

    @Override
    public List<Reimbursement> getAll() {
        connectionService = new ConnectionService();
        String sql = "select * from reimbursement";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            List<Reimbursement> reimbursementList = new ArrayList<>();
            UserDao userDao = new UserDaoImpl();

            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                User employee;
                User manager;
                reimbursement.setId(rs.getInt("reimbursement_id"));
                reimbursement.setAmount(rs.getDouble("amount"));
                reimbursement.setResolved(rs.getBoolean("is_resolved"));
                reimbursement.setDescription(rs.getString("description"));
                reimbursement.setCreatedOn(rs.getTimestamp("created_on"));
                reimbursement.setUpdatedOn(rs.getTimestamp("updated_on"));
                employee = userDao.getUserById(rs.getInt("employee_id"));
                manager = userDao.getUserById(rs.getInt("manager_id"));
                reimbursement.setEmployee(employee);
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);
            }
            return reimbursementList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Reimbursement> getReimbursementsByEmployeeId(int employeeId) {
        connectionService = new ConnectionService();
        String sql = "select * from reimbursement where employee_id = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, employeeId);
            ResultSet rs = pstmt.executeQuery();
            List<Reimbursement> reimbursementList = new ArrayList<>();
            UserDao userDao = new UserDaoImpl();
            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                User employee;
                User manager;
                reimbursement.setId(rs.getInt("reimbursement_id"));
                reimbursement.setAmount(rs.getDouble("amount"));
                reimbursement.setResolved(rs.getBoolean("is_resolved"));
                reimbursement.setDescription(rs.getString("description"));
                reimbursement.setCreatedOn(rs.getTimestamp("created_on"));
                reimbursement.setUpdatedOn(rs.getTimestamp("updated_on"));
                employee = userDao.getUserById(rs.getInt("employee_id"));
                manager = userDao.getUserById(rs.getInt("manager_id"));
                reimbursement.setEmployee(employee);
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);
            }
            return reimbursementList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Reimbursement> getReimbursementsByManagerId(int managerId) {
        connectionService = new ConnectionService();
        String sql = "select * from reimbursement where manager_id = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, managerId);
            ResultSet rs = pstmt.executeQuery();
            List<Reimbursement> reimbursementList = new ArrayList<>();
            UserDao userDao = new UserDaoImpl();
            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                User employee;
                User manager;
                reimbursement.setId(rs.getInt("reimbursement_id"));
                reimbursement.setAmount(rs.getDouble("amount"));
                reimbursement.setResolved(rs.getBoolean("is_resolved"));
                reimbursement.setDescription(rs.getString("description"));
                reimbursement.setCreatedOn(rs.getTimestamp("created_on"));
                reimbursement.setUpdatedOn(rs.getTimestamp("updated_on"));
                employee = userDao.getUserById(rs.getInt("employee_id"));
                manager = userDao.getUserById(rs.getInt("manager_id"));
                reimbursement.setEmployee(employee);
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);
            }
            return reimbursementList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Reimbursement> getAllPendingReimbursement() {
        connectionService = new ConnectionService();
        String sql = "select * from reimbursement where is_resolved = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setBoolean(1,false);
            ResultSet rs = pstmt.executeQuery();
            List<Reimbursement> reimbursementList = new ArrayList<>();
            UserDao userDao = new UserDaoImpl();
            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                User employee;
                User manager;
                reimbursement.setId(rs.getInt("reimbursement_id"));
                reimbursement.setAmount(rs.getDouble("amount"));
                reimbursement.setResolved(rs.getBoolean("is_resolved"));
                reimbursement.setDescription(rs.getString("description"));
                reimbursement.setCreatedOn(rs.getTimestamp("created_on"));
                reimbursement.setUpdatedOn(rs.getTimestamp("updated_on"));
                employee = userDao.getUserById(rs.getInt("employee_id"));
                manager = userDao.getUserById(rs.getInt("manager_id"));
                reimbursement.setEmployee(employee);
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);
            }
            return reimbursementList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Reimbursement> getAllEmployeePendingReimbursement(int employeeId) {
        connectionService = new ConnectionService();
        String sql = "select * from reimbursement where is_resolved = ? AND employee_id = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setBoolean(1,false);
            pstmt.setInt(2,employeeId);
            ResultSet rs = pstmt.executeQuery();
            List<Reimbursement> reimbursementList = new ArrayList<>();
            UserDao userDao = new UserDaoImpl();
            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                User employee;
                User manager;
                reimbursement.setId(rs.getInt("reimbursement_id"));
                reimbursement.setAmount(rs.getDouble("amount"));
                reimbursement.setResolved(rs.getBoolean("is_resolved"));
                reimbursement.setDescription(rs.getString("description"));
                reimbursement.setCreatedOn(rs.getTimestamp("created_on"));
                reimbursement.setUpdatedOn(rs.getTimestamp("updated_on"));
                employee = userDao.getUserById(rs.getInt("employee_id"));
                manager = userDao.getUserById(rs.getInt("manager_id"));
                reimbursement.setEmployee(employee);
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);
            }
            return reimbursementList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Reimbursement> getAllResolvedReimbursement() {
        connectionService = new ConnectionService();
        String sql = "select * from reimbursement where is_resolved = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setBoolean(1,true);
            ResultSet rs = pstmt.executeQuery();
            List<Reimbursement> reimbursementList = new ArrayList<>();
            UserDao userDao = new UserDaoImpl();
            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                User employee;
                User manager;
                reimbursement.setId(rs.getInt("reimbursement_id"));
                reimbursement.setAmount(rs.getDouble("amount"));
                reimbursement.setResolved(rs.getBoolean("is_resolved"));
                reimbursement.setDescription(rs.getString("description"));
                reimbursement.setCreatedOn(rs.getTimestamp("created_on"));
                reimbursement.setUpdatedOn(rs.getTimestamp("updated_on"));
                employee = userDao.getUserById(rs.getInt("employee_id"));
                manager = userDao.getUserById(rs.getInt("manager_id"));
                reimbursement.setEmployee(employee);
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);
            }
            return reimbursementList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Reimbursement> getAllEmployeeResolvedReimbursement(int employeeId) {
        connectionService = new ConnectionService();
        String sql = "select * from reimbursement where is_resolved = ? AND employee_id = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setBoolean(1,true);
            pstmt.setInt(2,employeeId);
            ResultSet rs = pstmt.executeQuery();
            List<Reimbursement> reimbursementList = new ArrayList<>();
            UserDao userDao = new UserDaoImpl();
            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                User employee;
                User manager;
                reimbursement.setId(rs.getInt("reimbursement_id"));
                reimbursement.setAmount(rs.getDouble("amount"));
                reimbursement.setResolved(rs.getBoolean("is_resolved"));
                reimbursement.setDescription(rs.getString("description"));
                reimbursement.setCreatedOn(rs.getTimestamp("created_on"));
                reimbursement.setUpdatedOn(rs.getTimestamp("updated_on"));
                employee = userDao.getUserById(rs.getInt("employee_id"));
                manager = userDao.getUserById(rs.getInt("manager_id"));
                reimbursement.setEmployee(employee);
                reimbursement.setManager(manager);
                reimbursementList.add(reimbursement);
            }
            return reimbursementList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Reimbursement getReimbursementById(int reimbursementId) {
        connectionService = new ConnectionService();
        String sql = "select * from reimbursement where reimbursement_id = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, reimbursementId);
            ResultSet rs = pstmt.executeQuery();
            UserDao userDao = new UserDaoImpl();
            while (rs.next()) {
                Reimbursement reimbursement = new Reimbursement();
                User employee;
                User manager;
                reimbursement.setId(rs.getInt("reimbursement_id"));
                reimbursement.setAmount(rs.getDouble("amount"));
                reimbursement.setResolved(rs.getBoolean("is_resolved"));
                reimbursement.setDescription(rs.getString("description"));
                reimbursement.setCreatedOn(rs.getTimestamp("created_on"));
                reimbursement.setUpdatedOn(rs.getTimestamp("updated_on"));
                employee = userDao.getUserById(rs.getInt("employee_id"));
                manager = userDao.getUserById(rs.getInt("manager_id"));
                reimbursement.setEmployee(employee);
                reimbursement.setManager(manager);
                return reimbursement;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean createReimbursement(Reimbursement reimbursement, int employeeId) {
        connectionService = new ConnectionService();
        boolean createSuccessful = false;
        String sql = "insert into reimbursement(amount, description, created_on, employee_id) " +
                "values (?, ?, ?, ?);";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());

            pstmt.setDouble(1, reimbursement.getAmount());
            pstmt.setString(2, reimbursement.getDescription());
            pstmt.setTimestamp(3, timestamp);
            pstmt.setInt(4, employeeId);

            createSuccessful = pstmt.executeUpdate() > 0;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return createSuccessful;
    }

    @Override
    public boolean updateReimbursement(int reimbursementId, int managerId) {
        connectionService = new ConnectionService();
        boolean updateSuccessful = false;
        String sql = "update reimbursement set is_resolved = ?, updated_on = ?, manager_id = ? where reimbursement_id = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            pstmt.setBoolean(1, true);
            pstmt.setTimestamp(2, timestamp);
            pstmt.setInt(3, managerId);
            pstmt.setInt(4, reimbursementId);
            updateSuccessful = pstmt.executeUpdate() > 0 ;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return updateSuccessful;
    }

}
