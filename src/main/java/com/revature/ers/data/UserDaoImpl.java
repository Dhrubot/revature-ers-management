package com.revature.ers.data;

import com.revature.ers.model.User;
import com.revature.ers.model.UserRole;
import com.revature.ers.service.ConnectionService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class UserDaoImpl implements UserDao{
    private ConnectionService connectionService;


    @Override
    public List<User> getAllEmployees() {
        connectionService = new ConnectionService();
        String sql = "select * from users where user_role = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, UserRole.EMPLOYEE.toString().toLowerCase());
            ResultSet rs = pstmt.executeQuery();
            List<User> userList = new ArrayList<>();

            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("user_id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setUsername(rs.getString("username"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("user_password"));
                user.setUserRole(UserRole.valueOf(rs.getString("user_role").toUpperCase()));
                userList.add(user);
            }
            return userList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User getUserById(int id) {
        connectionService = new ConnectionService();
        String sql = "select * from users where user_id = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            User user = new User();
            while(rs.next()) {
                user.setId(rs.getInt("user_id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setUsername(rs.getString("username"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("user_password"));
                user.setUserRole(UserRole.valueOf(rs.getString("user_role").toUpperCase()));
            }
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User getUserByUsernameAndPassword(String username, String password) {
        connectionService = new ConnectionService();
        String sql = "select * from users where username = ? and user_password = ?;";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet rs = pstmt.executeQuery();
            User user = new User();
            while(rs.next()) {
                user.setId(rs.getInt("user_id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setUsername(rs.getString("username"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("user_password"));
                user.setUserRole(UserRole.valueOf(rs.getString("user_role").toUpperCase()));
            }
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<User> getUsersByRole(String role) {
        connectionService = new ConnectionService();
        String sql = "select * from users where user_role = ?";
        try (Connection connection = connectionService.getConnection()) {
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, role);
            ResultSet rs = pstmt.executeQuery();
            List<User> userList = new ArrayList<>();

            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("user_id"));
                user.setFirstName(rs.getString("first_name"));
                user.setLastName(rs.getString("last_name"));
                user.setUsername(rs.getString("username"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("user_password"));
                user.setUserRole(UserRole.valueOf(rs.getString("user_role").toUpperCase()));
                userList.add(user);
            }
            return userList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
