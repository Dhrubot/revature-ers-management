package com.revature.ers.data;

import com.revature.ers.model.Reimbursement;
import com.revature.ers.model.User;

import java.util.List;

public interface ReimbursementDao {
    public List<Reimbursement> getAll();
    public List<Reimbursement> getReimbursementsByEmployeeId(int id);
    public List<Reimbursement> getReimbursementsByManagerId(int id);
    public List<Reimbursement> getAllPendingReimbursement();
    public List<Reimbursement> getAllEmployeePendingReimbursement(int id);
    public List<Reimbursement> getAllResolvedReimbursement();
    public List<Reimbursement> getAllEmployeeResolvedReimbursement(int id);
    public Reimbursement getReimbursementById(int id);
    public boolean createReimbursement(Reimbursement reimbursement, int employeeId);
    public boolean updateReimbursement(int reimbursementId, int managerId);
}
