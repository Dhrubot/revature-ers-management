package com.revature.ers.data;

import com.revature.ers.model.User;

import java.util.List;

public interface UserDao {
    public List<User> getAllEmployees();
    public User getUserById(int id);
    public User getUserByUsernameAndPassword(String username, String password);
    public List<User> getUsersByRole(String role);
}
