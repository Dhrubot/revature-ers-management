package com.revature.ers.model;

public enum UserRole {
    MANAGER, EMPLOYEE
}
