package com.revature.ers.model;

import java.sql.Timestamp;
import java.util.Objects;

public class Reimbursement {

    private int id;
    private User employee;
    private User manager;
    private double amount;
    private boolean isResolved;
    private String description;
    private Timestamp createdOn;
    private Timestamp updatedOn;

    public Reimbursement() {
    }

    public Reimbursement(int id) {
        this.id = id;
    }

    public Reimbursement(int id, User employee, User manager, double amount, boolean isResolved, String description, Timestamp createdOn, Timestamp updatedOn) {
        this.id = id;
        this.employee = employee;
        this.manager = manager;
        this.amount = amount;
        this.isResolved = isResolved;
        this.description = description;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getEmployee() {
        return employee;
    }

    public void setEmployee(User employee) {
        this.employee = employee;
    }

    public User getManager() {
        return manager;
    }

    public void setManager(User manager) {
        this.manager = manager;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isResolved() {
        return isResolved;
    }

    public void setResolved(boolean resolved) {
        isResolved = resolved;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reimbursement)) return false;
        Reimbursement that = (Reimbursement) o;
        return id == that.id && Double.compare(that.amount, amount) == 0 && isResolved == that.isResolved && employee.equals(that.employee) && Objects.equals(manager, that.manager) && Objects.equals(description, that.description) && Objects.equals(createdOn, that.createdOn) && Objects.equals(updatedOn, that.updatedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, employee, manager, amount, isResolved, description, createdOn, updatedOn);
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "id=" + id +
                ", employee=" + employee +
                ", manager=" + manager +
                ", amount=" + amount +
                ", isResolved=" + isResolved +
                ", description='" + description + '\'' +
                ", createdOn=" + createdOn +
                ", updatedOn=" + updatedOn +
                '}';
    }
}
