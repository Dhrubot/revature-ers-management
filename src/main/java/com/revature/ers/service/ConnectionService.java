package com.revature.ers.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionService {
    private Connection connection;

    public ConnectionService() {
        super();

        try {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            String url;
            String host = System.getenv("POSTGRES_HOST");

            if (host == null) {
                url = "jdbc:postgresql://localhost:5432/postgres";
            } else {
                url = "jdbc:postgresql://"+ host + ":5432/Revature_ERS";
            }
            String username = System.getenv("POSTGRES_USERNAME");
            String password = System.getenv("POSTGRES_PASSWORD");
            connection = DriverManager.getConnection(url, username, password);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

}
