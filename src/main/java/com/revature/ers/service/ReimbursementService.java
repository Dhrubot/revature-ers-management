package com.revature.ers.service;

import com.revature.ers.data.ReimbursementDao;
import com.revature.ers.data.ReimbursementDaoImpl;
import com.revature.ers.model.Reimbursement;
import com.revature.ers.model.User;
import com.revature.ers.model.UserRole;

import java.util.List;

public class ReimbursementService {

    ReimbursementDao reimbursementDao = new ReimbursementDaoImpl();

    public List<Reimbursement> getAllReimbursements() {
        return reimbursementDao.getAll();
    }

    public List<Reimbursement> getAllEmployeeReimbursements(User employee) {
        return reimbursementDao.getReimbursementsByEmployeeId(employee.getId());
    }

    public List<Reimbursement> getAllEmployeePendingReimbursements(User employee) {
        return reimbursementDao.getAllEmployeePendingReimbursement(employee.getId());
    }

    public List<Reimbursement> getAllEmployeeResolvedReimbursements(User employee) {
        return reimbursementDao.getAllEmployeeResolvedReimbursement(employee.getId());
    }

    public List<Reimbursement> getAllManagerReimbursements(User manager) {
        return reimbursementDao.getReimbursementsByManagerId(manager.getId());
    }

    public boolean createReimbursement(Double amount, String description, User user) {
        Reimbursement reimbursement = new Reimbursement();
        reimbursement.setAmount(amount);
        reimbursement.setDescription(description);
        return reimbursementDao.createReimbursement(reimbursement, user.getId());
    }

    public boolean approveReimbursement(Reimbursement reimbursement, User manager) {
        if (UserRole.MANAGER.equals(manager.getUserRole())){
            return reimbursementDao.updateReimbursement(reimbursement.getId(), manager.getId());
        }
        return false;
    }

    public List<Reimbursement> getAllPendingReimbursements() {
        return reimbursementDao.getAllPendingReimbursement();
    }

    public Reimbursement getReimbursementById(int id) {return reimbursementDao.getReimbursementById(id);}

    public List<Reimbursement> getAllResolvedReimbursements() {
        return reimbursementDao.getAllResolvedReimbursement();
    }
}
