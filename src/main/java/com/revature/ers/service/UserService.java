package com.revature.ers.service;

import com.revature.ers.data.UserDao;
import com.revature.ers.data.UserDaoImpl;
import com.revature.ers.model.User;

import java.util.List;

public class UserService {

    UserDao userDao = new UserDaoImpl();

    public List<User> getAllEmployees() {
        return userDao.getAllEmployees();
    }

    public User getUserByCredentials(String userName, String password) {
        if (userName == null || userName.isEmpty() || password == null || password.isEmpty()) {
            return null;
        }

        return userDao.getUserByUsernameAndPassword(userName, password);
    }

    public  User getUserById(Integer id) {
        return userDao.getUserById(id);
    }

}
