document.getElementById("home").addEventListener("click", goToHome);
let newReimbursement = document.getElementById("new-reimbursement").addEventListener("click", goToCreateReimbursement);
document.getElementById("pending-reimbursements").addEventListener("click", goToPendingReimbursements);
document.getElementById("resolved-reimbursements").addEventListener("click", goToResolvedReimbursements);
document.getElementById("profile").addEventListener("click", goToProfile);
document.getElementById("logout").addEventListener("click", logoutUser);
const token = sessionStorage.getItem("token");

//const BASE_URL = "http://localhost:8080/revature-ers-management"
const BASE_URL = "http://13.58.47.140:8080/revature-ers-management"


function goToCreateReimbursement() {
    if (token && token.split(":")[1] === "EMPLOYEE") {
        window.location.href = BASE_URL + "/static/employee/createReimbursement.html";
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}

function goToHome() {
    if (token && token.split(":")[1] === "EMPLOYEE") {
        window.location.href = BASE_URL + "/static/employee/home.html";
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}
function goToPendingReimbursements() {
    if (token && token.split(":")[1] === "EMPLOYEE") {
        window.location.href = BASE_URL + "/static/employee/pendingReimbursements.html";
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}

function goToResolvedReimbursements() {
    if (token && token.split(":")[1] === "EMPLOYEE") {
        window.location.href = BASE_URL + "/static/employee/resolvedReimbursements.html";
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}

function goToProfile() {
    if (token && token.split(":")[1] === "EMPLOYEE") {
        window.location.href = BASE_URL + "/static/employee/profile.html";
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}

function logoutUser() {
    if (token) {
        fetch(BASE_URL + "/logout")
            .then(res => {
                sessionStorage.clear();
            })
        window.location.href = BASE_URL + "/auth"
    }
}

