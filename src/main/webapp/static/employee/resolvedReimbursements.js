window.onload = async () => {
    if (!token) {
        window.location.href = BASE_URL + "/auth"
    } else if (token1 && token1.split(":")[1] !== "EMPLOYEE") {
        window.location.href = BASE_URL + "/static/errorPage.html"
    } else {
        fetchResolvedReimbursements();
    }
};
const token1 = sessionStorage.getItem("token");

let displayReimbursements = reimbursements => {
    const columnHeadings = ['Requested By', 'amount', 'description', 'status', "approved by", 'Request Date', "resolved date"];
    const columnCount = columnHeadings.length;
    const rowCount = reimbursements.length;


    const header = table.createTHead();

    let row = header.insertRow(-1);

    for (let i = 0; i < columnCount; i++) {

        let headerCell = document.createElement('th');

        headerCell.innerText = columnHeadings[i].toUpperCase();

        row.appendChild(headerCell);

    }

    let tBody = document.createElement('tbody');

    table.appendChild(tBody);

    // Add the data rows to the table body.

    for (let i = 0; i < reimbursements.length; i++) { // each row

        row = tBody.insertRow(-1);

        let objFields = ['employeeName', 'amount', 'description', 'status', 'resolvedBy', 'createdOn', "updatedOn"]


        let obj = reimbursements[i];

        objFields.forEach(function (field) {
            let cell = row.insertCell(-1);
            if (field === "employeeName") {
                cell.innerText = nameTitleCase(reimbursements[i].employee.firstName, reimbursements[i].employee.lastName)
            } else if (field === 'resolvedBy' && reimbursements[i].manager) {
                cell.innerText = nameTitleCase(reimbursements[i].manager.firstName, reimbursements[i].manager.lastName)
            }
            else if (field === 'status') {
                cell.innerText = reimbursements[i].resolved ? 'APPROVED' : 'PENDING';
                cell.style.color = reimbursements[i].resolved ? 'GREEN' : 'RED';
            }
            else if (field === "amount") {
                let money = new Number(reimbursements[i].amount);
                cell.innerText = `$ ${money.toFixed(2)}`;
            }
            else if (field === "createdOn" || field === "updatedOn") {
                let date = new Date(reimbursements[i][field]);
                cell.innerText = (date.getMonth() + 1) + "-" + date.getDate() + "-" + date.getFullYear()
            } else {
                cell.innerText = reimbursements[i][field];
            }
        })
    }
}

function nameTitleCase(firstname, lastname) {
    return firstname[0].toUpperCase() + firstname.substring(1)
        + " " +
        lastname[0].toUpperCase() + lastname.substring(1)
}

function fetchResolvedReimbursements() {
    if (token1 && token1.split(":")[1] === "EMPLOYEE") {
        fetch(BASE_URL + "/reimbursements", { headers: { "Authorization": token1, "filter": "resolved" } })
            .then(res => res.json())
            .then(data => displayReimbursements(data))
    } else {
        window.location.href = BASE_URL + "/auth"
    }
}