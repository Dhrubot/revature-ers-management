document.getElementById("create-reimb-btn").addEventListener("click", createReimbursement);
const token1 = sessionStorage.getItem("token");

document.onload = function () {
    if (!token) {
        window.location.href = BASE_URL + "/auth"
    } else if (token.split(":")[1] !== "EMPLOYEE") {
        window.location.href = BASE_URL + "/static/errorPage";
    }
};

function createReimbursement(event) {
    event.preventDefault();
    const errorDiv = document.getElementById("error-div");
    errorDiv.hidden = true;
    const amount = document.getElementById("amount").value;
    const description = document.getElementById("description").value;

    if (token1 && token1.split(":")[1] === "EMPLOYEE") {
        fetch(BASE_URL + "/reimbursements", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                "Authorization": token1,
            },
            body: JSON.stringify(
                {
                    amount,
                    description
                }
            )
        })
            .then(res => res.json())
            .then(data => {
                if (data.created) {
                    window.location.href = BASE_URL + "/static/employee/pendingReimbursements.html"
                } else {
                    errorDiv.hidden = false;
                    errorDiv.innerText = "New Request Reimbursement Unsuccessful. Try again Later."
                }
            })
    } else {
        window.location.href = BASE_URL + "/auth"
    }

}