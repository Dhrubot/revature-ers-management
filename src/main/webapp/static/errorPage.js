document.getElementById("back-button").addEventListener("click", redirectToHome);
const BASE_URL= "http://13.58.47.140:8080/revature-ers-management"
//const BASE_URL = "http://localhost:8080/revature-ers-management"
const token = sessionStorage.getItem("token");


function redirectToHome() {
    if (token) {
        if (token.split(":")[1] === "MANAGER") {
            window.location.href = BASE_URL + "/static/manager/home.html";
        } else if (token.split(":")[1] === "EMPLOYEE") {
            window.location.href = BASE_URL + "/static/employee/home.html";
        }
    } else {
        window.location.href = BASE_URL + "/auth";
    }
}