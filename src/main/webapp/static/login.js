// respond to clicking the login button by:
document.getElementById("login-btn").addEventListener("click", attemptLogin);


const BASE_URL_LOGIN = "http://13.58.47.140:8080/revature-ers-management"
//const BASE_URL_LOGIN = "http://localhost:8080/revature-ers-management"
function attemptLogin(){
    const errorDiv = document.getElementById("error-div");
    errorDiv.hidden = true;

    // get input values from input fields
    const username = document.getElementById("username-input").value;
    const password = document.getElementById("password-input").value;

    const xhr = new XMLHttpRequest();
    xhr.open("POST", BASE_URL_LOGIN + "/auth"); // ready state changes 0->1

    // define onreadystatechange callback for the xhr object (check for readystate 4)
    xhr.onreadystatechange = function(){
        if(xhr.readyState===4){
            // look at status code (either 401 or 200)
            // if the status code is 401 - indicate to the user that their credentials are invalid
            if(xhr.status===401){
                errorDiv.hidden = false;
                errorDiv.innerHTML = "Username/Password combination <br /> doesn't match";
            } else if (xhr.status===200){
                // if the status code is 200 - get auth token from response and store it in browser, navigate to another page
                const token = xhr.getResponseHeader("Authorization");
                sessionStorage.setItem("token", token);
                if (token.split(":")[1] === "MANAGER") {
                    window.location.href= BASE_URL_LOGIN + "/static/manager/home.html";
                } else if (token.split(":")[1] === "EMPLOYEE") {
                    window.location.href= BASE_URL_LOGIN + "/static/employee/home.html";
                } else {
                   errorDiv.hidden = false;
                   errorDiv.innerHTML = "Username/Password combination <br /> doesn't match";
                }
            } else {
                errorDiv.hidden = false;
                errorDiv.innerText = "Internal Error";
            }
        }
    }

    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    // send request, with the username and password in the request body
    const requestBody = `username=${username}&password=${password}`;
    xhr.send(requestBody); // ready state changes 1->2, 2->3, 3-4
}
