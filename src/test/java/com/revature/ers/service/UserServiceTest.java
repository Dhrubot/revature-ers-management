package com.revature.ers.service;

import com.revature.ers.data.ReimbursementDao;
import com.revature.ers.data.ReimbursementDaoImpl;
import com.revature.ers.data.UserDao;
import com.revature.ers.data.UserDaoImpl;
import com.revature.ers.model.User;
import com.revature.ers.model.UserRole;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserServiceTest {
    private ReimbursementDao reimbursementDao = new ReimbursementDaoImpl();
    private UserService userService = new UserService();

    @Test
    public void getUserByCredentialsTest() {
        User actualUser = new User();
        actualUser.setId(10);
        actualUser.setFirstName("test");
        actualUser.setLastName("test");
        actualUser.setUsername("test");
        actualUser.setEmail("test");
        actualUser.setPassword("test");
        actualUser.setUserRole(UserRole.MANAGER);

        User expectedUser =userService.getUserByCredentials("test", "test");

        assertEquals(expectedUser, actualUser);
    }
}
