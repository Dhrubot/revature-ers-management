package com.revature.ers.data;

import com.revature.ers.model.User;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UserDaoImplTest {
    private ReimbursementDao reimbursementDao = new ReimbursementDaoImpl();
    private UserDao userDao = new UserDaoImpl();

    @Test
    public void getUsersByRoleTest() {
        List<User> actual = userDao.getUsersByRole("employee");

        assertTrue(actual.size() > 0);
    }
}
