package com.revature.ers.data;

import com.revature.ers.data.ReimbursementDao;
import com.revature.ers.data.ReimbursementDaoImpl;
import com.revature.ers.data.UserDao;
import com.revature.ers.data.UserDaoImpl;
import com.revature.ers.model.Reimbursement;
import com.revature.ers.model.User;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ReimbursementDaoImplTest {
    private ReimbursementDao reimbursementDao = new ReimbursementDaoImpl();
    private UserDao userDao = new UserDaoImpl();

    @Test
    public void getReimbursementByValidId(){
        Reimbursement actual = reimbursementDao.getReimbursementById(1);
        User employee = userDao.getUserById(1);
        User manager = userDao.getUserById(2);
        Reimbursement expected = new Reimbursement();
        expected.setId(1);
        expected.setAmount(200.00);
        expected.setEmployee(employee);
        expected.setManager(manager);
        expected.setResolved(true);
        expected.setDescription("dhrubo first req");
        expected.setCreatedOn(actual.getCreatedOn());
        expected.setUpdatedOn(actual.getUpdatedOn());
        assertEquals(expected, actual);
    }

    @Test
    public void getReimbursementByInValidId(){
        Reimbursement actual = reimbursementDao.getReimbursementById(1500);
        assertNull(actual);
    }

    @Test
    public void getReimbursementsByEmployeeId(){
        List<User> users = userDao.getUsersByRole("employee");
        List<Reimbursement> actual = reimbursementDao.getReimbursementsByEmployeeId(users.get(0).getId());
        assertNotNull(actual);
    }

}
