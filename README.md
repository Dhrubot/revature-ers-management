# Revature Ers Management

## Project Description

This is an Employee Reimbursement Management System where an Employee can request
for a Reimbursement and a Manager can Approve a Reimbursement request.

## Technologies Used

* JAVA - 1.8
* Postgresql - 42.2.24
* JavaScript - ES2015
* Servlets
* JDBC
* HTML
* CSS
* Bootstrap
* AJAX
* JUnit
* log4j

## Features

List of features ready and TODOs for future development
* Secured Login and Navigating System for both Employee and Manager
* Employees can submit reimbursement requests
* Employees can view the status of previously submitted requests
* Managers can view all past requests from all users
* Managers can approve pending reimbursement requests

To-do list:
* Password encryption
* Implement a Mail Service
* User Registration

## Getting Started

* git clone `https://gitlab.com/Dhrubot/revature-ers-management.git`
* Following Environment Variables needed to be set up. POSTGRES_HOST, POSTGRES_USERNAME, POSTGRES_PASSWORD

## Usage

User need to have an existing account to log in to the system. Based on the role User
will be redirected to either the Employee Homepage or the Manager Homepage and can navigate to pages
to Create Reimbursements(Employee option only), see all the pending Reimbursements(Manager option only), see Resolved Reimbursement
and go to profile.



## License

This project uses the following license: [<https://opensource.org/licenses/MIT>](<link>).
